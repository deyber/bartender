package com.pisteapp.bartender.Data.Retrofit;

import static com.pisteapp.bartender._Tools.Conexions.apiRest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static RetrofitClient instance;
    Retrofit retrofit;

    public static RetrofitClient getInstance(){
        if( instance == null){
            instance = new RetrofitClient();
        }
        return instance;
    }
    public RetrofitClient(){
        retrofit = new Retrofit
                        .Builder()
                        .baseUrl(apiRest)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
    }

    public Retrofit getRetrofit(){
        return retrofit;
    }

}
