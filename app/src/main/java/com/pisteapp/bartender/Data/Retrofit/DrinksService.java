package com.pisteapp.bartender.Data.Retrofit;
import com.pisteapp.bartender.Model.Pojo.Drinks;

import retrofit2.Call;
import retrofit2.http.GET;

public interface DrinksService {

    @GET("search.php?s=margarita")
    Call<Drinks> listDrinks();
}
