package com.pisteapp.bartender.Model.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Drink {
    @SerializedName("strDrink")
    @Expose
    String name;

    @SerializedName("strInstructions")
    @Expose
    String detail;

    @SerializedName("strDrinkThumb")
    @Expose
    String img;

    public Drink(String name, String detail, String img) {
        this.name = name;
        this.detail = detail;
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
