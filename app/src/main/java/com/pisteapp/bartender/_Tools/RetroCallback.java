package com.pisteapp.bartender._Tools;

public interface RetroCallback {
     void onResponse(Object object);
     void onFailure(Object object);
}
