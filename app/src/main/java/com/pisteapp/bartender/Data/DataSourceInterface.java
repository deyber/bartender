package com.pisteapp.bartender.Data;
import com.pisteapp.bartender.Model.Pojo.Drinks;
import retrofit2.Call;

public interface DataSourceInterface {
    
        Call<Drinks> getList(String drink);
    }