package com.pisteapp.bartender.UI;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pisteapp.bartender.Model.Pojo.Drinks;
import com.pisteapp.bartender.ViewModel.RetrofitVM;
import com.pisteapp.bartender._Tools.RetroCallback;
import com.pisteapp.bartender.databinding.FragmentDashBinding;


public class DashFragment extends Fragment {

    FragmentDashBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentDashBinding.inflate(getLayoutInflater());
        return  binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RetrofitVM vm = new ViewModelProvider(this).get(RetrofitVM.class);

        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Navigation.findNavController(v).navigate(R.id.action_dashFragment_to_detailFragment);
                 vm.getLista(new RetroCallback() {
                     @Override
                     public void onResponse(Object object) {
                         Drinks drinks = (Drinks) object;
                     }

                     @Override
                     public void onFailure(Object object) {
                         Log.d("mensaje", "tenemos un error al buscar los datos");
                     }
                 });
            }
        });
    }
}