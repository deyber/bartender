package com.pisteapp.bartender.ViewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.pisteapp.bartender.Data.DataSource;
import com.pisteapp.bartender.Model.Pojo.Drinks;
import com.pisteapp.bartender._Tools.RetroCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitVM extends ViewModel {
    DataSource dataSource;
    MutableLiveData<Drinks> drinks;

    public RetrofitVM() {
        this.dataSource = new DataSource();
    }

    public void getLista(RetroCallback retroCallback){
        Call<Drinks> call = dataSource.getList("margarita");
        call.enqueue(new Callback<Drinks>() {
            @Override
            public void onResponse(Call<Drinks> call, Response<Drinks> response) {
                if(response.isSuccessful()){
                    retroCallback.onResponse(response.body());
                }
            }

            @Override
            public void onFailure(Call<Drinks> call, Throwable t) {
                retroCallback.onFailure(true);
            }
        });
    }

    private MutableLiveData<Drinks> getMutableDrinks(){
        if(drinks==null){
            drinks = new MutableLiveData<>();
        }
        return drinks;
    }
}
