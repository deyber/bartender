package com.pisteapp.bartender.Data;

import com.pisteapp.bartender.Data.Retrofit.DrinksService;
import com.pisteapp.bartender.Data.Retrofit.RetrofitClient;
import com.pisteapp.bartender.Model.Pojo.Drinks;
import retrofit2.Call;

public class DataSource implements  DataSourceInterface{

    RetrofitClient retrofitClient;
    DrinksService drinksService;

    public DataSource() {
        retrofitClient = RetrofitClient.getInstance();
        drinksService = retrofitClient.getRetrofit().create(DrinksService.class);
    }

    @Override
    public Call<Drinks> getList(String drink) {
        return  drinksService.listDrinks();
    }
}
